import axios from 'axios'

const getUrl = path => `https://identitytoolkit.googleapis.com/v1/accounts:${path}?key=${process.env.FIREBASE_API_KEY}`

export const createAccount = ({ email, password }) => {
    const url = getUrl('signUp')
    const params = { email, password, returnSecureToken: true }
    return axios.post(url, params)
}

export const login = ({ email, password }) => {
    const url = getUrl('signInWithPassword')
    const params = { email, password, returnSecureToken: true }
    return axios.post(url, params)
}

export const sendPasswordResetCode = ({ email }) => {
    const url = getUrl('sendOobCode')
    const params = { email, requestType: 'PASSWORD_RESET' }
    return axios.post(url, params)
}

export const verifyPasswordResetCode = ({ email, code }) => {
    const url = getUrl('resetPassword')
    const params = { oobCode: code }
    return axios.post(url, params)
}

export const resetPassword = ({ email, password, password_confirmation, code }) => {
    const url = getUrl('resetPassword')
    const params = { newPassword: password, oobCode: code }
    return axios.post(url, params)
}

export const deleteAccount = ({ idToken }) => {
    const url = getUrl('delete')
    const params = { idToken }
    return axios.post(url, params)
}
