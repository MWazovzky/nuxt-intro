import client from '~/services/api'

const path = '/categories'

export const index = () => {
    return client.get(`${path}.json`).then(res => {
        const data = Object.keys(res.data).map(key => ({ key, ...res.data[key] }))
        return { data }
    })
}

export const store = (payload, token) => {
    return client.post(`${path}.json?auth=${token}`, payload).then(res => {
        const data = { ...payload, key: res.data.name }
        return { data }
    })
}

export const update = (key, payload, token) => {
    return client.put(`${path}/${key}.json?auth=${token}`, payload)
}

export const destroy = (key, token) => {
    return client.delete(`${path}/${key}.json?auth=${token}`)
}
