import * as auth from './auth'
import * as categories from './categories'
import * as posts from './posts'
import * as tags from './tags'
import * as users from './users'

export { auth, categories, posts, tags, users }
