import client from '~/services/api'

const path = '/posts'

export const index = () => {
    return client.get(`${path}.json`).then(res => {
        const data = Object.keys(res.data).map(key => ({ ...res.data[key], id: key }))
        return { data }
    })
}

export const store = (payload, token) => {
    return client.post(`${path}.json?auth=${token}`, payload).then(res => {
        const data = { ...payload, id: res.data.name }
        return { data }
    })
}

export const update = (id, payload, token) => {
    return client.put(`${path}/${id}.json?auth=${token}`, payload)
}

export const destroy = (id, token) => {
    return client.delete(`${path}/${id}.json?auth=${token}`)
}
