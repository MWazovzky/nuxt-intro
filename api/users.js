import client from '~/services/api'

const path = '/users'

export const index = () => {
    return client.get(`${path}.json`).then(res => {
        const data = Object.keys(res.data).map(key => ({ key, ...res.data[key] }))
        return { data }
    })
}

export const findOne = ({ email }) => {
    return client.get(`${path}.json?orderBy="email"&equalTo="${email}"`).then(({ data }) => {
        const items = Object.keys(data).map(key => ({ key, ...data[key] }))
        return { data: items[0] }
    })
}

export const store = payload => {
    return client.post(`${path}.json`, payload).then(res => {
        const data = { ...payload, key: res.data.name }
        return { data }
    })
}

export const update = (key, payload) => {
    return client.put(`${path}/${key}.json`, payload)
}

export const destroy = key => {
    return client.delete(`${path}/${key}.json`)
}
