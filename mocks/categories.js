const categories = [
    { id: 1, name: 'Development'},
    { id: 2, name: 'Testing'},
    { id: 3, name: 'Devops'},
    { id: 4, name: 'Algorithms' },
]

export default categories;
