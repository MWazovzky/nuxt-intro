const tags =  [
    { id: 1, name: 'PHP'},
    { id: 2, name: 'Laravel'},
    { id: 3, name: 'JavaScript'},
    { id: 4, name: 'Vue'},
    { id: 5, name: 'Nuxt'},
    { id: 6, name: 'SSR'},
]

export default tags;
