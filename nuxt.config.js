import colors from 'vuetify/es5/util/colors'
require('dotenv').config()

export default {
    mode: 'universal',
    /*
     ** Headers of the page
     */
    head: {
        titleTemplate: '%s - ' + process.env.npm_package_name,
        title: process.env.npm_package_name || '',
        meta: [
            { charset: 'utf-8' },
            { name: 'viewport', content: 'width=device-width, initial-scale=1' },
            { hid: 'description', name: 'description', content: process.env.npm_package_description || '' }
        ],
        link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }]
    },
    /*
     ** Customize the progress-bar color
     */
    loading: { color: '#fff' },
    /*
     ** Global CSS
     */
    css: ['~/assets/styles/app.css'],
    /*
     ** Plugins to load before mounting the App
     */
    plugins: [
        '~/plugins/axios-client',
        '~/plugins/google-maps',
        '~/plugins/v-mask',
        '~/plugins/base-components',
        '~/plugins/notification'
    ],
    /*
     ** Nuxt.js dev-modules
     */
    buildModules: ['@nuxtjs/vuetify', '@nuxtjs/dotenv'],
    /*
     ** Nuxt.js modules
     */
    modules: [
        // Doc: https://axios.nuxtjs.org/usage
        '@nuxtjs/axios',
        // Doc: https://github.com/nuxt-community/dotenv-module
        '@nuxtjs/dotenv'
    ],
    /*
     ** Axios module configuration
     ** See https://axios.nuxtjs.org/options
     */
    axios: {
        baseURL: process.env.apiURL
    },
    env: {
        apiURL: process.env.API_URL,
        FIREBASE_API_KEY: process.env.FIREBASE_API_KEY,
        GOOGLE_MAPS_API_KEY: process.env.GOOGLE_MAPS_API_KEY
    },
    /*
     ** vuetify module configuration
     ** https://github.com/nuxt-community/vuetify-module
     */
    vuetify: {
        customVariables: ['~/assets/styles/variables.scss'],
        theme: {
            dark: false,
            themes: {
                dark: {
                    primary: colors.blue.darken2,
                    accent: colors.grey.darken3,
                    secondary: colors.amber.darken3,
                    info: colors.teal.lighten1,
                    warning: colors.amber.base,
                    error: colors.deepOrange.accent4,
                    success: colors.green.accent3
                }
            }
        }
    },
    /*
     ** Build configuration
     */
    build: {
        /*
         ** You can extend webpack config here
         */
        extend(config, ctx) {
            transpile: [/^vue2-google-maps($|\/)/]
        }
    },
    router: {
        extendRoutes(routes, resolve) {
            routes.push({
                name: 'login',
                path: '/login',
                component: resolve(__dirname, 'pages/auth.vue'),
                props: { name: 'login' }
            })
            routes.push({
                name: 'register',
                path: '/register',
                component: resolve(__dirname, 'pages/auth.vue'),
                props: { name: 'register' }
            })
            routes.push({
                name: 'forgot-password',
                path: '/forgot-password',
                component: resolve(__dirname, 'pages/auth.vue'),
                props: { name: 'forgot-password' }
            })
        }
    }
}
