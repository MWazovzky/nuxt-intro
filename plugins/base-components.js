import Vue from 'vue'
import Modal from '@/components/Base/Modal'
import ButtonBase from '@/components/Base/ButtonBase'

Vue.component('x-modal', Modal)
Vue.component('x-button', ButtonBase)
