import Vue from 'vue'

export default ({ store }) => {
    Vue.prototype.notify = (text, color = null) => store.dispatch('notification/notify', { text, color })
}
