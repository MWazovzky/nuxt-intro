const methods = ['get', 'post', 'put', 'patch', 'delete']
let client
let service = {}

export function setClient (newClient) {
    client = newClient
}

methods.forEach((method) => {
  service[method] = function () {
    if (!client) throw new Error('Client not installed')
    return client[method].apply(null, arguments)
  }
})

export default service
