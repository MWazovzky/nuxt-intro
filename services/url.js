export function appendUrlParams(url, params) {
    let [path, query] = url.split('?')
    let segments = query ? query.split('&') : []

    Object.keys(params).forEach(key => {
        segments.push(`${encodeURIComponent(key)}=${encodeURIComponent(params[key])}`)
    })

    query = segments.join('&')
    
    return `${path}?${query}`
}
