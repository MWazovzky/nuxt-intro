import Cookie from 'js-cookie'
import { auth as authApi } from '@/api'

export const state = () => ({
    user: null,
    token: null
})

export const getters = {
    user: state => state.user,
    token: state => state.token,
    isAuthenticated: state => state.token != null
}

export const mutations = {
    setUser(state, user) {
        state.user = user
    },
    setToken(state, token) {
        state.token = token
    }
}

export const actions = {
    register({ dispatch }, { email, password, name }) {
        return authApi.createAccount({ email, password }).then(() => {
            dispatch('users/create', { email, name }, { root: true })
        })
    },

    login({ commit, dispatch }, { email, password }) {
        let token
        let expiresIn
        return authApi
            .login({ email, password })
            .then(({ data }) => {
                token = data.idToken
                expiresIn = Number(data.expiresIn) * 1000
                return dispatch('users/find', { email }, { root: true })
            })
            .then(({ data }) => {
                commit('setUser', data)
                commit('setToken', token)

                if (localStorage) {
                    localStorage.setItem('user', JSON.stringify(data))
                    localStorage.setItem('token', token)
                    localStorage.setItem('tokenExpirationTime', now() + expiresIn)
                }

                Cookie.set('user', JSON.stringify(data))
                Cookie.set('token', token)
                Cookie.set('tokenExpirationTime', now() + expiresIn)
            })
    },

    sendPasswordResetCode(ctx, { email }) {
        return authApi.sendPasswordResetCode({ email })
    },

    verifyPasswordResetCode(ctx, { email, code }) {
        return authApi.verifyPasswordResetCode({ email, code })
    },

    resetPassword(ctx, { email, password, password_confirmation, code }) {
        return authApi.resetPassword({ password, code })
    },

    logout({ commit }) {
        commit('setToken', null)
        commit('setUser', null)

        localStorage.removeItem('user')
        localStorage.removeItem('token')
        localStorage.removeItem('tokenExpirationTime')

        Cookie.remove('user')
        Cookie.remove('token')
        Cookie.remove('tokenExpirationTime')
    },

    initAuth({ commit, dispatch }, req) {
        let user
        let token
        let tokenExpirationTime

        if (req) {
            user = decode(getCookie(req, 'user'))
            token = getCookie(req, 'token')
            tokenExpirationTime = getCookie(req, 'tokenExpirationTime')
        } else {
            user = (x => x && JSON.parse(x))(localStorage.getItem('user'))
            token = localStorage.getItem('token')
            tokenExpirationTime = Number(localStorage.getItem('tokenExpirationTime'))
        }

        if (!user || !token || now() > tokenExpirationTime) {
            return dispatch('logout')
        }

        commit('setUser', user)
        commit('setToken', token)
    }
}

function now() {
    return new Date().getTime()
}

function getCookie(req, name) {
    return req.headers.cookie
        .split(';')
        .find(c => c.trim().startsWith(name))
        .split('=')[1]
}

function decode(cookie) {
    return cookie && JSON.parse(cookie.replace(/%2C/g, ',').replace(/%22/g, '"'))
}
