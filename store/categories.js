import { categories as api } from '@/api'

export const state = () => ({
    categories: []
})

export const getters = {
    get: state => state.categories
}

export const mutations = {
    set(state, categories) {
        state.categories = categories
    },

    append(state, category) {
        state.categories.push(category)
    },

    modify(state, category) {
        const index = state.categories.findIndex(item => item.key === category.key)
        if (index === -1) throw new Error('Category not found. Can not modify.')
        state.categories.splice(index, 1, category)
    },

    remove(state, category) {
        const index = state.categories.findIndex(item => item.key === category.key)
        if (index === -1) throw new Error('Category not found. Can not remove.')
        state.categories.splice(index, 1)
    }
}

export const actions = {
    load({ commit }) {
        return api
            .index()
            .then(({ data }) => commit('set', data))
            .catch(err => console.log(err))
    },

    create({ commit, getters, rootGetters }, category) {
        const { name } = category
        const id = 1 + getters.get.reduce((max, category) => (max && max > category.id ? max : category.id), null)
        return api
            .store({ id, name }, rootGetters['auth/token'])
            .then(({ data }) => commit('append', data))
            .catch(err => console.log(err))
    },

    update({ commit, rootGetters }, category) {
        const { key, ...params } = category
        return api
            .update(key, params, rootGetters['auth/token'])
            .then(() => commit('modify', category))
            .catch(err => console.log(err))
    },

    destroy({ commit, rootGetters }, category) {
        return api
            .destroy(category.key, rootGetters['auth/token'])
            .then(() => commit('remove', category))
            .catch(err => console.log(err))
    }
}
