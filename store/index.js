export const actions = {
    // Asynchronous nuxtServerInit action must return a Promise or leverage async/await
    nuxtServerInit({ commit, dispatch }) {
        return Promise.all([
            dispatch('users/load'),
            dispatch('categories/load'),
            dispatch('tags/load'),
            dispatch('posts/load')
        ])
    }
}
