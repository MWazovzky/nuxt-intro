export const state = () => ({
    state: false,
    color: 'pink',
    text: ''
})

export const getters = {
    getState: state => state.state,
    getColor: state => state.color,
    getText: state => state.text
}

export const mutations = {
    setState(state, value) {
        state.state = value
    },
    setColor(state, color) {
        state.color = color
    },
    setText(state, text) {
        state.text = text
    }
}

export const actions = {
    notify({ commit }, { text, color = 'pink' }) {
        commit('setText', text)
        commit('setColor', color)
        commit('setState', true)
    }
}
