import { posts as api } from '@/api'

export const state = () => ({
    posts: []
})

export const getters = {
    get: state => state.posts,

    find: state => slug => state.posts.find(post => post.slug === slug)
}

export const mutations = {
    set(state, posts) {
        state.posts = posts
    },

    append(state, post) {
        state.posts.push(post)
    },

    modify(state, post) {
        const index = state.posts.findIndex(item => item.key === post.key)
        if (index === -1) throw new Error('Post not found. Can not update.')
        state.posts.splice(index, 1, post)
    },

    remove(state, post) {
        const index = state.posts.findIndex(item => item.key === post.key)
        if (index === -1) throw new Error('Post not found. Can not remove.')
        state.posts.splice(index, 1)
    }
}

export const actions = {
    load({ commit }) {
        return api.index().then(({ data }) => commit('set', data))
    },

    create({ commit, rootGetters }, post) {
        return api.store(post, rootGetters['auth/token']).then(({ data }) => commit('append', data))
    },

    update({ commit, rootGetters }, post) {
        const { id, ...params } = post
        return api.update(id, params, rootGetters['auth/token']).then(() => commit('modify', post))
    },

    destroy({ commit, rootGetters }, post) {
        return api.destroy(post.id, rootGetters['auth/token']).then(() => commit('remove', post))
    }
}
