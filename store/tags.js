import { tags as api } from '@/api'

export const state = () => ({
    tags: []
})

export const getters = {
    get: state => state.tags
}

export const mutations = {
    set(state, tags) {
        state.tags = tags
    },

    append(state, tag) {
        state.tags.push(tag)
    },

    modify(state, tag) {
        const index = state.tags.findIndex(item => item.key === tag.key)
        if (index === -1) throw new Error('Tag not found. Can not modify.')
        state.tags.splice(index, 1, tag)
    },

    remove(state, tag) {
        const index = state.tags.findIndex(item => item.key === tag.key)
        if (index === -1) throw new Error('Tag not found. Can not remove.')
        state.tags.splice(index, 1)
    }
}

export const actions = {
    load({ commit }) {
        return api
            .index()
            .then(({ data }) => commit('set', data))
            .catch(err => console.log(err))
    },

    create({ commit, getters, rootGetters }, tag) {
        const { name } = tag
        const id = 1 + getters.get.reduce((max, tag) => (max && max > tag.id ? max : tag.id), null)
        return api
            .store({ id, name }, rootGetters['auth/token'])
            .then(({ data }) => {
                commit('append', data)
                return data
            })
            .catch(err => console.log(err))
    },

    update({ commit, rootGetters }, tag) {
        const { key, ...params } = tag
        return api
            .update(key, params, rootGetters['auth/token'])
            .then(() => commit('modify', tag))
            .catch(err => console.log(err))
    },

    destroy({ commit, rootGetters }, tag) {
        return api
            .destroy(tag.key, rootGetters['auth/token'])
            .then(() => commit('remove', tag))
            .catch(err => console.log(err))
    }
}
