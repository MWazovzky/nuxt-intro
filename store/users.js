import { users as api } from '@/api'

export const state = () => ({
    users: []
})

export const getters = {
    get: state => state.users
}

export const mutations = {
    set(state, users) {
        state.users = users
    },

    append(state, user) {
        state.users.push(user)
    },

    modify(state, user) {
        const index = state.users.findIndex(item => item.key === user.key)
        if (index === -1) throw new Error('User not found. Can not modify.')
        state.users.splice(index, 1, user)
    },

    remove(state, user) {
        const index = state.users.findIndex(item => item.key === user.key)
        if (index === -1) throw new Error('User not found. Can not remove.')
        state.users.splice(index, 1)
    }
}

export const actions = {
    load({ commit }) {
        return api
            .index()
            .then(({ data }) => commit('set', data))
            .catch(err => console.log(err))
    },

    find(ctx, { email }) {
        return api.findOne({ email })
    },

    create({ commit, getters }, user) {
        const id = 1 + getters.get.reduce((max, user) => (max && max > user.id ? max : user.id), null)
        return api
            .store({ id, ...user })
            .then(({ data }) => commit('append', data))
            .catch(err => console.log(err))
    },

    update({ commit }, user) {
        const { key, ...params } = user
        return api
            .update(key, params)
            .then(() => commit('modify', user))
            .catch(err => console.log(err))
    },

    destroy({ commit }, user) {
        return api
            .destroy(user.key)
            .then(() => commit('remove', user))
            .catch(err => console.log(err))
    }
}
